function drawLEDIndicatorDefault(HTMLparentElementID, color) {
    var ledBorder = document.createElement("div");

    ledBorder.id = HTMLparentElementID + "_ledBorder";


    ledBorder.setAttribute("class", "led-border-medium");
    document.getElementById(HTMLparentElementID).appendChild(ledBorder);

   
    var ledLight = document.createElement("div");
    var ledLightClass;
    ledLight.setAttribute("class", "led-light-" + color + "-unknown");
    ledBorder.appendChild(ledLight);
    return ledLight;
}

function LEDIndicator(HTMLparentElementID){
    
    this.color = "green";
    this.HTMLLEDIndicatorElement = drawLEDIndicatorDefault(HTMLparentElementID, this.color);
    this.tag = new TagObject(new PLC(), "", "bool", this, updateLEDIndicator);

    this.parentID = HTMLparentElementID;

    this.setpriority = function (value) {
        this.priority = value;

        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0 && this.tag.type.length > 0
     && this.priority !== undefined) {
            this.bind();
        }
    }
    
       

   

    this.setlabel = function (value) {
        this.label = value;

        var labelDiv = document.getElementById("setLabel" + this.parentID);

        labelDiv.innerHTML = "";

        labelDiv.innerHTML = "</br>" + " "+ this.label;


    }

    this.setTagName = function(name){
        this.tag.tagName = name;
        
        if (this.label == undefined) {
            this.label = name;

            var labelDiv = document.createElement("div");

            labelDiv.id = "setLabel" + this.parentID;


            labelDiv.innerHTML = "</br>" + this.label;

            labelDiv.setAttribute("class", "ledIndiclabel right");

            this.HTMLLEDIndicatorElement.insertAdjacentElement('afterend', labelDiv);
        }

        if(this.tag.plc.AMSAddress.length > 0 && name.length > 0){
            this.bind();
        }
    }

    this.connectTag = function (plcObj, value) {

        this.tag = new TagObject(plcObj, "", "bool", this, updateLEDIndicator);
        this.tag.tagName = value;

        if (this.label == undefined) {
            this.label = name;

            var labelDiv = document.createElement("div");

            labelDiv.id = "setLabel" + this.parentID;


            labelDiv.innerHTML = "</br>" + this.label;

            labelDiv.setAttribute("class", "ledIndiclabel right");

            this.HTMLLEDIndicatorElement.insertAdjacentElement('afterend', labelDiv);
        }

        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0) {
            this.bind();
        }

    }


    this.setLabelPosition = function (value) {
        var labelDiv = document.getElementById("setLabel" + this.parentID);


        if (value == "top") {

            labelDiv.setAttribute("class", "ledIndiclabel top");

        }

        if (value == "bottom") {

            labelDiv.setAttribute("class", "ledIndiclabel bottom");

        }

        if (value == "left") {

            labelDiv.setAttribute("class", "ledIndiclabel left");

        }

        if (value == "right") {

            labelDiv.setAttribute("class", "ledIndiclabel right");

        }



    }


    this.setPLCAMSAddress = function(AMSAddress){
        this.tag.plc.AMSAddress = AMSAddress;
        if(AMSAddress.length > 0 && this.tag.tagName.length > 0){
            this.bind();
        }
    }
    this.setPLCRuntimePort = function(port){
        this.tag.plc.runtimePort = port;
        if(this.tag.plc.AMSAddress.length > 0 && this.tag.tagName.length > 0){
            this.bind();
        }
    }
    
    this.bind = function () {
        if (this.priority === "medium") {
            mediumPollingTagArray.push(this.tag);
        }

        if (this.priority === "fast") {
            fastPollingTagArray.push(this.tag);
        }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.tag);
        }

        //default polling
        if (this.priority == undefined) {
            fastPollingTagArray.push(this.tag);

        }
    }
    
    this.setColor = function(color){
        if(color === "blue"){
            this.color = "blue";
        }
        else if(color === "red"){
            this.color = "red";
        }
        else if(color === "green"){
            this.color = "green";
        }
        else if(color === "yellow"){
            this.color = "yellow";
        }
        //TODO else throw exception.
      this.HTMLLEDIndicatorElement.setAttribute("class", "led-light-" + color + "-unknown");
    }


    this.setSize = function (value) {
        if (value == "small") {
            var htmlElm = document.getElementById(this.parentID + "_ledBorder");
            htmlElm.setAttribute("class", "led-border-small");

        }

        if (value == "medium") {
            var htmlElm = document.getElementById(this.parentID + "_ledBorder");
            htmlElm.setAttribute("class", "led-border-medium");

        }

        if (value == "large") {
            var htmlElm = document.getElementById(this.parentID + "_ledBorder");
            htmlElm.setAttribute("class", "led-border-large");

        }


    }

}

function updateLEDIndicator(ledIndicatorObj, value) {
    var ledIndicatorHtmlElement = ledIndicatorObj.HTMLLEDIndicatorElement;
    var classPrefix = "led-light-" + ledIndicatorObj.color;
	if(value == true){
		ledIndicatorHtmlElement.setAttribute("class", classPrefix + "-on");
	}else if(value == false){
		ledIndicatorHtmlElement.setAttribute("class",  classPrefix + "-off");
	}else{
		ledIndicatorHtmlElement.setAttribute("class",  classPrefix + "-unknown");
	}
}