tinymce.init({
	/* replace textarea having class .tinymce with tinymce editor */
	
selector: "textarea.tinymce",
	
	

/* theme of the editor */
	
theme: "modern",
	
skin: "lightgray",
	
	

/* width and height of the editor */
	
width: "100%",
	
height: 300,
	
	

/* display statusbar */
	
statubar: true,
	
	

/* plugin */
	
plugins: [
		
"advlist autolink link image lists charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"save table contextmenu directionality emoticons template paste textcolor visualblocks fullpage textpattern "
	],



/* toolbar */
	
toolbar: "visualblocks | mybutton | insertfile undo redo | styleselect code | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",


visualblocks_default_state: true,

importcss_append: true,

	
setup: function(editor) {
    editor.addButton('mybutton', {
        type: 'menubutton',
        text: 'HMI Widgets',
        icon: false,
        menu: [{
            text: 'Led Indicator',
            onclick: function () {

                var ledID = parseInt(Math.random() * 205545);
                editor.insertContent('<span style="background-color: #ffff00;">***</span>');

                var divCont = tinymce.activeEditor.dom.createHTML('div', { id: ledID })

                editor.insertContent(divCont);
              
                var modal = document.getElementById('myLedConnect');
                var span = document.getElementsByClassName("close")[0];

                var submitBtn = document.getElementById('myLedConnect_btn');

                submitBtn.onclick = function () {
                    modal.style.display = "none";


                    //var s = tinymce.activeEditor.dom.createHTML('script');
                    //s.type = 'text/javascript';

                    //var code = '';

                    try {
                        //s.appendChild(document.createTextNode(code));
                     //   divCont.appendChild(s);
                    } catch (e) {
                        s.text = code;
                      //  divCont.appendChild(s);
                    }


                   //// var code = document.createElement('script');
                   // code.type = 'text/javascript';

                   // createdLedDiv.appendChild(code);

                    editor.notificationManager.open({
                        text: 'Led indicator added',
                        type: 'success',
                        timeout: 1000
                    });

                }

                span.onclick = function () {
                    modal.style.display = "none";
                }

                modal.style.display = "block";

               

            }
        }, {
            text: 'Momentary Button',
            onclick: function() {
                editor.insertContent('&nbsp;<em>Momentary Button</em>&nbsp;');

                
            }
        },

        {
            text: 'Regular Button',
            onclick: function () {
                editor.insertContent('&nbsp;<em>Regulor Button</em>&nbsp;');
            }
        }
        ]
    });
},


/* style */
	
style_formats: [
		
{title: "Headers", items: [
			{title: "Header 1", format: "h1"},
			{title: "Header 2", format: "h2"},
			{title: "Header 3", format: "h3"},
			{title: "Header 4", format: "h4"},
			{title: "Header 5", format: "h5"},
			{title: "Header 6", format: "h6"}
		]},
		{title: "Inline", items: [
			{title: "Bold", icon: "bold", format: "bold"},
			{title: "Italic", icon: "italic", format: "italic"},
			{title: "Underline", icon: "underline", format: "underline"},
			{title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
			{title: "Superscript", icon: "superscript", format: "superscript"},
			{title: "Subscript", icon: "subscript", format: "subscript"},
			{title: "Code", icon: "code", format: "code"}
		]},
		{title: "Blocks", items: [
			{title: "Paragraph", format: "p"},
			{title: "Blockquote", format: "blockquote"},
			{title: "Div", format: "div"},
			{title: "Pre", format: "pre"}
		]},
		{title: "Alignment", items: [
			{title: "Left", icon: "alignleft", format: "alignleft"},
			{title: "Center", icon: "aligncenter", format: "aligncenter"},
			{title: "Right", icon: "alignright", format: "alignright"},
			{title: "Justify", icon: "alignjustify", format: "alignjustify"}
		]}
]




});